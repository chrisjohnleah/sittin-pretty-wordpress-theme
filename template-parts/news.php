<?php
/**
 * The default template for displaying content news
 *
 * Used for news summary areas.
 *
 * @package SP-AG-ACL
 * @since sp-ag-cl 1.0.0
 */

?>

<div class="small-12 medium-6 columns">
    <div class="article-wrap">
        <article id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry '); ?> data-equalizer-watch>
            <header>
                <?php
                if (has_post_thumbnail()) :
                    the_post_thumbnail('sp-cl-ag-feature');
                endif;

                ?>
                <?php sp_cl_ag_entry_meta_date(); ?>
                <h3 class="subheader"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </header>
            <div class="entry-content">
                <?php the_excerpt(); ?>
            </div>
            <footer>
                <?php $tag = get_the_tags();
                if ($tag) : ?><p><?php the_tags(); ?></p><?php endif; ?>
            </footer>
        </article>
        <a href="<?php the_permalink(); ?>" class="button read-more">More Info</a>
    </div>
</div>
