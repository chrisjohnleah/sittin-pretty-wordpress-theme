<?php
/**
 * The default template for displaying content gallery
 *
 * Used for news summary areas.
 *
 * @package SP-AG-ACL
 * @since sp-ag-cl 1.0.0
 */

?>
<div class="small-12  medium-6 large-4 columns">
    <div class="article-wrap">
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </article>

    </div>
</div>