<?php
/**
 * The default template for displaying content video
 *
 * Used for news summary areas.
 *
 * @package SP-AG-ACL
 * @since sp-ag-cl 1.0.0
 */

?>
<div class="small-12  medium-6 columns">
    <div class="article-wrap">
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <?php do_action('foundationpress_page_before_entry_content'); ?>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
            <footer>
                <h3 class="subheader text-center margin-top"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </footer>
        </article>

    </div>
</div>