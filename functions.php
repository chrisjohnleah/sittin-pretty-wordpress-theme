<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Configure wordpress customizer  */
require_once( 'library/customizer.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );
if ( ! isset( $content_width ) )
    $content_width = 1280;

/**
 * Override the default WordPress video shortcode, injecting the post thumbnail as a
 * poster frame unless one has explicitly been set
 *
 * @param array $attr The shortcode attributes
 * @param str $content Content enclosed between open and closing video shortcode tags
 * @return str
 */
function grunwell_video_embed( $attr, $content='' ) {
    if ( ! isset( $attr['poster'] ) && has_post_thumbnail() ) {
        // This uses a custom, 16:9 image size named 'poster' but could be any size
        $poster = wp_get_attachment_image_src( get_post_thumbnail_id(), 'poster' );
        $attr['poster'] = $poster['0'];
    }

    return wp_video_shortcode( $attr, $content );
}
add_shortcode( 'video', 'grunwell_video_embed' );