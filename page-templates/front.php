<?php
/*
Template Name: Front
*/
get_header(); ?>

    <header id="front-hero" role="banner">
        <?php echo do_shortcode('[rev_slider alias="home"]'); ?>
    </header>

<?php do_action('foundationpress_before_content'); ?>

<?php
// WP_Query arguments
$args = array(
    'tax_query' => array(
        array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array(
                'post-format-aside',
                'post-format-audio',
                'post-format-chat',
                'post-format-gallery',
                'post-format-image',
                'post-format-link',
                'post-format-quote',
                'post-format-status',
                'post-format-video'
            ),
            'operator' => 'NOT IN'
        )
    ),
    'post_type' => array('post'),
    'post_status' => array('publish'),
    'nopaging' => false,
    'posts_per_page' => 2,
    'order' => 'DESC',
    'cache_results' => true,
    'update_post_meta_cache' => true,
    'update_post_term_cache' => true,
);

// The Query
$news = new WP_Query($args);
?>
    <section class="row" id="news" data-equalizer data-equalize-on="medium">
        <h2 class="text-center">News</h2>
        <?php if ($news->have_posts()) : ?>

            <?php while ($news->have_posts()):
                $news->the_post(); ?>
                <?php get_template_part('template-parts/news', get_post_format()); ?>
            <?php endwhile; ?>

            <div class="small-12 columns text-center"><a href="<?php echo esc_url(get_permalink(get_page_by_title('News'))); ?>"
                                        class="button section">See all news</a></div>

            <?php

        else: ?>
            <div class="small-12 columns">
                <div class="article-wrap">
                    <article id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry '); ?>>
                        <header>
                            <h4 class="subheader text-center">No news to display.</h4>
                        </header>
                    </article>
                </div>
            </div>
        <?php endif;

        // Restore original Post Data
        wp_reset_postdata();
        ?>
    </section>
    <section class="row" id="gigs" data-equalizer data-equalize-on="medium">
        <h2 class="text-center">Upcoming Gigs</h2>

        <div class="small-12 columns">
            <a href="http://www.songkick.com/artists/7016739" class="songkick-widget" data-theme="light"
               data-track-button="on" data-detect-style="true" data-background-color="#000000"></a>
            <script src="//widget.songkick.com/widget.js"></script>
        </div>
    </section>
<?php
$args = array(
    'tax_query' => array(
        array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array(
                'post-format-gallery'
            ),
            'operator' => 'IN'
        )
    ),
    'post_type' => array('post'),
    'post_status' => array('publish'),
    'nopaging' => false,
    'posts_per_page' => '3',
    'order' => 'DESC',
    'cache_results' => true,
    'update_post_meta_cache' => true,
    'update_post_term_cache' => true,

);

// The Query
$gallery = new WP_Query($args);
?>
    <section class="row" id="gallery">
        <h2 class="text-center">Gallery</h2>
        <?php if ($gallery->have_posts()) : ?>

            <?php while ($gallery->have_posts()):
                $gallery->the_post(); ?>
                <?php get_template_part('template-parts/content-gallery', get_post_format()); ?>
            <?php endwhile; ?>

            <div class="small-12 columns text-center"><a
                    href="<?php echo esc_url(get_permalink(get_page_by_title('Gallery'))); ?>"
                    class="button section">See more</a></div>


            <?php

        else: ?>
            <div class="small-12 columns">
                <div class="article-wrap">
                    <article id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry '); ?>>
                        <header>
                            <h4 class="subheader text-center">No gallery items to display.</h4>
                        </header>
                    </article>
                </div>
            </div>
        <?php endif;

        // Restore original Post Data
        wp_reset_postdata();
        ?>
    </section>
<?php
$args = array(
    'tax_query' => array(
        array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array(
                'post-format-video'
            ),
            'operator' => 'IN'
        )
    ),
    'post_type' => array('post'),
    'post_status' => array('publish'),
    'nopaging' => false,
    'posts_per_page' => '2',
    'order' => 'DESC',
    'cache_results' => true,
    'update_post_meta_cache' => true,
    'update_post_term_cache' => true,

);

// The Query
$videos = new WP_Query($args);
?>
    <section class="row" id="gallery">
        <h2 class="text-center">Videos</h2>
        <?php if ($videos->have_posts()) : ?>

            <?php while ($videos->have_posts()):
                $videos->the_post(); ?>
                <?php get_template_part('template-parts/content-video', get_post_format()); ?>

            <?php endwhile; ?>

            <div class="small-12 columns text-center"><a
                    href="<?php echo esc_url(get_permalink(get_page_by_title('Videos'))); ?>"
                    class="button section">See more videos</a></div>


            <?php

        else: ?>
            <div class="small-12 columns">
                <div class="article-wrap">
                    <article id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry '); ?>>
                        <header>
                            <h4 class="subheader text-center">No videos to display.</h4>
                        </header>
                    </article>
                </div>
            </div>
        <?php endif;

        // Restore original Post Data
        wp_reset_postdata();
        ?>
    </section>
<?php do_action('foundationpress_after_content'); ?>
<?php get_footer();

