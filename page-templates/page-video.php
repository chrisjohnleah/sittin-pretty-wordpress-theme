<?php
/*
Template Name: Videos
*/
get_header();

$args = array(
    'tax_query' => array(
        array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array(
                'post-format-video'
            ),
            'operator' => 'IN'
        )
    ),
    'post_type' => array('post'),
    'post_status' => array('publish'),
    'nopaging' => false,
    'posts_per_page' => '12',
    'order' => 'DESC',
    'cache_results' => true,
    'update_post_meta_cache' => true,
    'update_post_term_cache' => true,

);

$videos = new WP_Query($args);
?>
<div id="page-full-width" role="main">
    <header>
        <h1 class="entry-title text-center"><?php the_title(); ?></h1>
    </header>
    <?php if ($videos->have_posts()) : ?>

        <?php while ($videos->have_posts()):
            $videos->the_post(); ?>
            <div class="small-12 columns">
                <div class="article-wrap">
                    <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                        <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
                        <div class="entry-content text-center">
                            <?php the_content(); ?>
                        </div>
                        <footer>
                            <h3 class=" text-center margin-top"><?php the_title(); ?></h3>
                        </footer>
                    </article>

                </div>
            </div>
        <?php endwhile; ?>

        <?php

    else: ?>
        <div class="small-12 columns">
            <div class="article-wrap">
                <article id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry '); ?>>
                    <header>
                        <h4 class="subheader text-center">No videos to display.</h4>
                    </header>
                </article>
            </div>
        </div>
    <?php endif; ?>

<?php get_footer();