<?php
/**
 * The default template for displaying content news
 *
 * Used for news summary areas.
 *
 * @package SP-AG-ACL
 * @since sp-ag-cl 1.0.0
 */


get_header();

    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array(
                    'post-format-aside',
                    'post-format-audio',
                    'post-format-chat',
                    'post-format-gallery',
                    'post-format-image',
                    'post-format-link',
                    'post-format-quote',
                    'post-format-status',
                    'post-format-video'
                ),
                'operator' => 'NOT IN'
            )
        ),
    'post_type' => array('post'),
    'post_status' => array('publish'),
    'nopaging' => false,
    'posts_per_page' => '8',
    'order' => 'DESC',
    'cache_results' => true,
    'update_post_meta_cache' => true,
    'update_post_term_cache' => true,

    );

    $news = new WP_Query($args);
    ?>
    <section class="row" id="news" data-equalizer data-equalize-on="medium">
        <header>
            <h1 class="entry-title text-center">News</h1>
        </header>
        <?php if ($news->have_posts()) : ?>

            <?php while ($news->have_posts()):
                $news->the_post(); ?>
                <?php get_template_part('template-parts/news', get_post_format()); ?>
            <?php endwhile;?>
            <?php if ( function_exists( 'foundationpress_pagination' ) ) : foundationpress_pagination();  elseif ( is_paged() ) : ?>
                <nav id="post-nav">
                    <div class="post-previous button read-more"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
                    <div class="post-next button read-more"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
                </nav>
            <?php endif; ?>
        <?php else:
            get_template_part('template-parts/content', 'none');
        endif;

        // Restore original Post Data
        wp_reset_postdata();
        ?>
    </section>

<?php get_footer();
