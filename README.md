# Sittin Pretty Wordpress Theme
Built using FoundationPress as a quick theme ready for the bands tour in Nov 2016!

Typical band site with tour dates from SongKick, Blog, Gallery, and Woocommerce for Merch.

http://www.wearesittinpretty.com/

^Since been let loose to the client. Beware some things aren't as they were originally designed or intended.

Designed by Adam Gorton.