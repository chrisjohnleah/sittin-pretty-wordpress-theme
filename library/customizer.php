<?php
/**
 * Create Logo Setting and Upload Control
 */
function sp_cl_ag_customizer_settings($wp_customize) {
// add a setting for the site logo
    $wp_customize->add_setting('sp_cl_ag_theme_logo');
// Add a control to upload the logo
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sp_cl_ag_theme_logo',
        array(
            'label' => 'Upload Logo',
            'section' => 'title_tagline',
            'settings' => 'sp_cl_ag_theme_logo',
        ) ) );
}
add_action('customize_register', 'sp_cl_ag_customizer_settings');
