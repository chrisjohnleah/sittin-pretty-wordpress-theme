<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
    <!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
<?php do_action('foundationpress_after_body'); ?>

<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas') : ?>
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <?php get_template_part('template-parts/mobile-off-canvas'); ?>
<?php endif; ?>

<?php do_action('foundationpress_layout_start'); ?>

    <header id="masthead" class="site-header" role="banner">
        <div class="title-bar text-center" data-responsive-toggle="site-navigation">
            <button class="menu-icon float-right" type="button" data-toggle="mobile-menu"></button>
            <div class="title-bar-title float-left">
                <?php
                // check to see if the logo exists and add it to the page
                if (get_theme_mod('sp_cl_ag_theme_logo')) : ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <img src="<?php echo get_theme_mod('sp_cl_ag_theme_logo'); ?>"
                             alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="logo">
                    </a>
                <?php // add a fallback if the logo doesn't exist
                else : ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a>

                <?php endif; ?>

            </div>
        </div>
        <div class="top-bar-container" data-sticky-container>
            <div class="sticky" data-sticky data-options="anchor: page; marginTop: 0; stickyOn: medium;"
                 style="width:100%; z-index:2">

                <nav id="site-navigation" class="main-navigation top-bar" role="navigation">
                    <div class="row column">
                        <div class="top-bar-left">
                            <ul class="menu">
                                <li class="home">
                                    <?php
                                    // check to see if the logo exists and add it to the page
                                    if (get_theme_mod('sp_cl_ag_theme_logo')) : ?>

                                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                            <img src="<?php echo get_theme_mod('sp_cl_ag_theme_logo'); ?>"
                                                 alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                                                 class="logo">
                                        </a>

                                    <?php // add a fallback if the logo doesn't exist
                                    else : ?>
                                        <a href="<?php echo esc_url(home_url('/')); ?>"
                                           rel="home"><?php bloginfo('name'); ?></a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </div>
                        <div class="top-bar-right">
                            <?php foundationpress_top_bar_r(); ?>

                            <?php if (!get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') === 'topbar') : ?>
                                <?php get_template_part('template-parts/mobile-top-bar'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <section class="container">
<?php do_action('foundationpress_after_header');
